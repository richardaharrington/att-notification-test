/**
 * Module dependencies.
 */
var https = require('./http-request.js');

var attOAuthInfo = {
/*    att_oauth_access_token: 'P7297DPX2h9zbI7GvuOHlVCiX9o0YfBd' */
    att_oauth_access_token: 'rk7AeD7seswRsBFw57gRS4UIeamluUI5'
};

function connectToSTOMP () {
    console.log('Beginning STOMP Streaming Process');

    /* createMessageIndex(); */
    getNotificationConnection();
}

function createMessageIndex () {
    var options = {
        hostname: 'api.att.com',
        method: 'POST',
        path: '/myMessages/v2/messages/index',
        headers: {
            'accept': 'application/json',
            'authorization': 'Bearer ' + attOAuthInfo.att_oauth_access_token
        }
    };

    https.httpsRequest(
        options, 
        //onDownloadCallback
        null, 
        //onCompleteCallback
        function (data, status) {
            console.log("Create Message Index status: " + status);
            getNotificationConnection()
        }, 
        //onErrorCallback
        function(error) {
            processError(error);
        }
    );
}

function getNotificationConnection () {
    // get notification connection
    var options = {
        hostname: 'api.att.com',
        method: 'GET',
        path: '/myMessages/v2/notificationConnectionDetails?queues=TEXT',
        headers: {
            'accept': 'application/json',
            'authorization': 'Bearer ' + attOAuthInfo.att_oauth_access_token
        }
    };

    https.httpsRequest(
        options, 
        //onDownloadCallback
        null, 
        //onCompleteCallback
        function (data, status) {
            var d = JSON.parse(data);

            if (d.notificationConnectionDetails != null) {
                createWebsocketConnectionToService(d.notificationConnectionDetails);
            } else {
                processError({ error: 'Notification details were not sent.'});
            }
        }, 
        //onErrorCallback
        function(error) {
            processError(error);
        }
    );
}

function createWebsocketConnectionToService (data) {
    console.log(data);

    var connectCallback = function () {
        console.log('Connection Established');
    };

    var errorCallback = function (error) {
        console.log('Error: ' + error);
    };

    var headers = {
        login: data.username,
        passcode: data.password
    };

    var WebSocket = require('faye-websocket');
    
    console.log('Trying to connect to Att at: ' + data.wssUrl);
    var ws = new WebSocket.Client(data.wssUrl);

    ws.on('open', function(event) {
        console.log('connected to att');
        ws.send("CONNECT\naccept-version:1.1\nlogin:" + data.username + "\npasscode:" + data.password + "\n\n\u0000");
    });
    
    ws.on('message', function(event) {
      console.log('message', event.data);
      if (event.data.substr(0,9) == 'CONNECTED') {
        var msg = "SUBSCRIBE\ndestination:" + data.queues.text + "\n\n\u0000";
        console.log('subscribe request',msg)
        ws.send(msg);
      }
    });
      
    ws.on('close', function(event) {
        console.log('disconnected');
    });
    ws.on('error', function(error) {
        console.log('Error');
        console.log(error);
    });
    /*
    console.log('Trying to connect to Websocket.org at: wss://echo.websocket.org');
    var wsTest = new WebSocket.Client('wss://echo.websocket.org');

    wsTest.on('open', function(event) {
        console.log('connected to wss://echo.websocket.org')
    });
    */
}

//Start
connectToSTOMP();
