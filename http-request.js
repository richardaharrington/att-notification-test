var https = require('https');

exports.httpsRequest = function(options, onDownloadCallback, onCompleteCallback, onErrorCallback) {
    var req = https.request(options, function(res) {
        res.setEncoding('utf8');

        var data = '';
        res.on('data', function(chunk) {
            data += chunk;
            if (onDownloadCallback !== null) {
                onDownloadCallback(chunk);
            }
        });

        res.on('end', function() {
            if (onCompleteCallback !== null) {
                onCompleteCallback(data, res.statusCode);
            }
        });
    });
    req.on('error', function(error) {
        if (onErrorCallback !== null) {
            onErrorCallback(error);
        }
    });

    req.end();
};